<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App;


//GET ALL CUSTOMERS
$app->get('/api/customers', function (Request $request, Response $response) {

    $sql= "SELECT * FROM customers";

    try {
            //Get db Object
            $db = new db();
            //connect
            $db = $db->connect();
            
            $stmt = $db->query($sql);
            $customers =$stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
            echo json_encode($customers);
        
    } catch(PDOException $e){
            echo '{"error:{"text": '.$e->getMessage().'}';
    }

});

//GET SINGLE CUSTOMER

$app->get('/api/customer/{id}', function (Request $request, Response $response) {
        $id = $request->getAttribute('id');

        $sql= "SELECT * FROM customers WHERE id=$id ";
    
        try {
                //Get db Object
                $db = new db();
                //connect
                $db = $db->connect();
                
                $stmt = $db->query($sql);
                $customer =$stmt->fetchAll(PDO::FETCH_OBJ);
                $db = null;
                echo json_encode($customer);
            
        } catch(PDOException $e){
                echo '{"error:{"text": '.$e->getMessage().'}';
        }
});
//ADD SINGLE CUSTOMER
$app->post('/api/customer/add', function (Request $request, Response $response) {
    $first_name = $request->getParam('first_name');
    $last_name = $request->getParam('last_name');
    $phone = $request->getParam('phone');
    $email = $request->getParam('email');
    $address = $request->getParam('address');
    $city = $request->getParam('city');
    $country = $request->getParam('country');
    
    $sql= "INSERT INTO customers (first_name,last_name,phone,email,address,city,country) VALUES
    (:first_name,:last_name,:phone,:email,:address,:city,:country)";

    try {
            //Get db Object
            $db = new db();
            //connect
            $db = $db->connect();
            
            $stmt = $db->prepare($sql);

            $stmt->bindParam(':first_name',$first_name);
            $stmt->bindParam(':last_name',$last_name);
            $stmt->bindParam(':phone',$phone);
            $stmt->bindParam(':email',$email);
            $stmt->bindParam(':address',$address);
            $stmt->bindParam(':city',$city);
            $stmt->bindParam(':country',$country);
            $stmt->execute();

            echo'{"notice":{"text:"customer added"}';

        
    } catch(PDOException $e){
            echo '{"error:{"text": '.$e->getMessage().'}';
    }
});
//UPDATE SINGLE CUSTOMER
$app->put('/api/customer/update/{id}', function (Request $request, Response $response) {
    $id = $request->getAttribute('id');
    $first_name = $request->getParam('first_name');
    $last_name = $request->getParam('last_name');
    $phone = $request->getParam('phone');
    $email = $request->getParam('email');
    $address = $request->getParam('address');
    $city = $request->getParam('city');
    $country = $request->getParam('country');
    
    $sql= "UPDATE customers SET
            first_name = :first_name,
            last_name = :last_name,
            phone = :phone,
            email = :email,
            address = :address,
            city = :city,
            country = :country 
            WHERE id = $id";

    try {
            //Get db Object
            $db = new db();
            //connect
            $db = $db->connect();
            
            $stmt = $db->prepare($sql);

            $stmt->bindParam(':first_name',$first_name);
            $stmt->bindParam(':last_name',$last_name);
            $stmt->bindParam(':phone',$phone);
            $stmt->bindParam(':email',$email);
            $stmt->bindParam(':address',$address);
            $stmt->bindParam(':city',$city);
            $stmt->bindParam(':country',$country);
            $stmt->execute();

            echo'{"notice":{"text:"customer updated"}';

        
    } catch(PDOException $e){
            echo '{"error:{"text": '.$e->getMessage().'}';
    }
});
//DELETE SINGLE CUSTOMER
$app->delete('/api/customer/delete/{id}', function (Request $request, Response $response) {
    $id = $request->getAttribute('id');
    $first_name = $request->getParam('first_name');
    $last_name = $request->getParam('last_name');
    $phone = $request->getParam('phone');
    $email = $request->getParam('email');
    $address = $request->getParam('address');
    $city = $request->getParam('city');
    $country = $request->getParam('country');
    
    $sql= "DELETE FROM customers  
            WHERE id = $id";

    try {
        //Get db Object
        $db = new db();
        //connect
        $db = $db->connect();
        
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $db = null;
        echo'{"notice":{"text:"customer DELETED"}';
    } catch(PDOException $e){
        echo '{"error:{"text": '.$e->getMessage().'}';
    }
});